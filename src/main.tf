# This is where you write your module code.
resource "random_string" "this" {
  length  = 10
  special = false
  upper   = false
}

locals {
  bucket_name = "s3-${var.context.zone}-${var.context.env_name}-${var.bucket_name}-${random_string.this.result}"
}

resource "aws_s3_bucket" "the_bucket" {
  bucket        = local.bucket_name
  force_destroy = var.force_destroy

  # checkov:skip=CKV_AWS_144: "Cross Region Replication is not mandatory"
  # checkov:skip=CKV_AWS_145: "KMS encryption is not mandatory for now."
  # checkov:skip=CKV_AWS_18: "S3 Access Logging is skipped for now."

  tags = merge(     
    {
      "Name" = local.bucket_name
    },
    var.additional_tags,
    local.context_tags
  )
}

resource "aws_s3_bucket_server_side_encryption_configuration" "sse" {
  bucket = aws_s3_bucket.the_bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_versioning" "s3_bucket_versioning" {
  bucket = aws_s3_bucket.the_bucket.id
  versioning_configuration {
    status = var.versioning
  }
}

resource "aws_s3_bucket_acl" "bucket_acl" {
  bucket = aws_s3_bucket.the_bucket.id
  acl    = "private"
}

resource "aws_s3_bucket_public_access_block" "public_access_block" {
  bucket = aws_s3_bucket.the_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
